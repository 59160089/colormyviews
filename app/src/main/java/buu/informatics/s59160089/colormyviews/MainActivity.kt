package buu.informatics.s59160089.colormyviews

import android.graphics.Color
import android.view.View
import android.widget.TextView
import buu.informatics.s59160089.colormyviews.R
import kotlinx.android.synthetic.main.activity_main.view.*

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setListenner()
    }

    private fun makeColored (view : View) {
        when(view.id){
            R.id.box_one_text -> view.setBackgroundColor(Color.DKGRAY)
            R.id.box_two_text -> view.setBackgroundColor(Color.GRAY)
            R.id.box_three_text -> view.setBackgroundColor(Color.BLUE)
            R.id.box_four_text -> view.setBackgroundColor(Color.MAGENTA)
            R.id.box_five_text -> view.setBackgroundColor(Color.BLUE)

            R.id.button_red -> box_three_text.setBackgroundResource(R.color.my_red)
            R.id.button_yellow -> box_four_text.setBackgroundResource(R.color.my_yellow)
            R.id.button_green -> box_five_text.setBackgroundResource(R.color.my_green)
        }
    }

    private fun setListenner () {

        val box_one_text = findViewById<View>(R.id.box_one_text)
        val box_two_text = findViewById<View>(R.id.box_two_text)
        val box_three_text = findViewById<View>(R.id.box_three_text)
        val box_four_text = findViewById<View>(R.id.box_four_text)
        val box_five_text = findViewById<View>(R.id.box_five_text)

        val red_button = findViewById<Button>(R.id.button_red)
        val yellow_button = findViewById<Button>(R.id.button_yellow)
        val green_button = findViewById<Button>(R.id.button_green)

        val root_constraint_layout = findViewById<View>(R.id.constraint_layout)

        val clickableViews : List<View> = listOf(box_one_text,box_two_text,box_three_text,box_four_text,box_five_text , root_constraint_layout , red_button , yellow_button , green_button)

        for (item in clickableViews){
            item.setOnClickListener{
                makeColored(it)
            }
        }
    }
}
